package id.ac.ui.cs.advprog.tutorial1.strategy;

public class Squeak implements QuackBehavior{

	@Override
	public void quack() {
		System.out.println("Quackkkk k k k \nSorry my voice kinda pitchy");
	}
}
