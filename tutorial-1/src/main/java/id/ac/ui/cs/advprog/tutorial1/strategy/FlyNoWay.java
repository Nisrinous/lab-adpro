package id.ac.ui.cs.advprog.tutorial1.strategy;

public class FlyNoWay implements FlyBehavior{

	@Override
	public void fly() {
		System.out.println("Are you joking?! I'll just fly no where then.");
	}
    
}
