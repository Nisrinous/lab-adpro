package id.ac.ui.cs.advprog.tutorial1.strategy;

public class FlyWithWings implements FlyBehavior{

	@Override
	public void fly() {
		System.out.println("My wings are sooooo damn beautiful, aren't they?");
	}
}
