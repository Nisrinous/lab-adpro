package id.ac.ui.cs.advprog.tutorial1.strategy;

public class MallardDuck extends Duck {

	public MallardDuck() {
		setFlyBehavior(new FlyRocketPowered());
		setQuackBehavior(new Quack());
	}
	
	@Override
	public void display() {
		System.out.println("Furrr on my head is totally green!");
	}
}
