package id.ac.ui.cs.advprog.tutorial1.strategy;

public abstract class Duck {

	private FlyBehavior flyBehavior;
    private QuackBehavior quackBehavior;
    
    public abstract void display();
	
	public void swim() {
		System.out.println("Now you can see me swimming!");
	}
	
    public void performFly() {
        flyBehavior.fly();
    }

    public void setFlyBehavior(FlyBehavior flyBehavior) {
		this.flyBehavior = flyBehavior;
	}

    public void performQuack() {
        quackBehavior.quack();
    }
    
	public void setQuackBehavior(QuackBehavior quackBehavior) {
		this.quackBehavior = quackBehavior;
	}
	
}
